#!/usr/bin/env python3

import argparse
import collections
import random
import sys

FMT = '<span style="font-size:{0}px;top:{1}px;left:{2}px;transform:rotate({3}deg);"></span>'
GRID = ((-200, 2200), (-200, 2200), 80)

def grid_coord(grid=GRID):
    x_vals, y_vals, cell = grid
    x_width = x_vals[1] - x_vals[0]
    y_width = y_vals[1] - y_vals[0]
    x_amt = int(x_width / cell)
    y_amt = int(y_width / cell)
    for i in range(x_amt):
        for j in range(y_amt):
            x = x_vals[0] + (i * cell) + random.randint(0, cell)
            y = y_vals[0] + (j * cell) + random.randint(0, cell)
            yield (x, y)

Values = collections.namedtuple('Values', ('font_size', 'rotate', 'x', 'y'))

def get_values(coords):
    while True:
        try:
            (x, y) = next(coords)
            font_size = random.randint(10, 40)
            rotate = random.randint(0, 360 - 1)
            yield Values(font_size, rotate, x, y)
        except StopIteration:
            break

def main(args):
    x, y = args.origin
    w, h = args.dimensions
    grid = ((x, x + w), (y, y + h), args.cell)

    famount = (w * h) / (args.cell * args.cell)
    amount = int(famount)
    if famount != amount:
        print('Warning: width and/or height not divisible by cell size', file=sys.stderr)

    coords = grid_coord(grid=grid)
    values = get_values(coords)
    for i in range(amount):
        try:
            v = next(values)
            print(FMT.format(v.font_size, v.x, v.y, v.rotate))
        except StopIteration:
            break

if __name__ == '__main__':
    def get_parser():
        parser = argparse.ArgumentParser()
        parser.add_argument('-c', '--cell', metavar='size', type=int, default=100,
                help='width and height of cell')
        parser.add_argument('-o', '--origin', metavar=('x', 'y'), type=int, nargs=2, default=(-200, -200),
                help='origin point')
        parser.add_argument('-d', '--dimensions', metavar=('width', 'height'), type=int, nargs=2, default=(3200, 4200),
                help='dimensions of grid')
        return parser
    args = get_parser().parse_args()
    main(args)
